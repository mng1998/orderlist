﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OrderNumber
{
    class Program
    {
        public static void Print(List<int> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                Console.Write(list[i] + " ");
            }
            Console.WriteLine();
        }
        static void Main(string[] args)
        {
            List<int> myList = new List<int>() { 10, 9, 3, 1, 100, 4, 101, 236, 5, 182, 469, 100, 207, 0, 11, 2, 3, 4, 94, 98, 46, 50, 67, 66, 68 };

            Console.WriteLine("My list:");
            Print(myList);


            List<int> tmpList = new List<int>();
            for (int i = 5; i >= 0; i--)
            {
                int maxNumbers = myList.Max();
                myList.Remove(maxNumbers);
                tmpList.Insert(0, maxNumbers);
            }

            tmpList.Sort();
            tmpList.Reverse();

            tmpList = tmpList.Concat(myList).ToList();

            Console.WriteLine("Ordered list:" );
            Print(tmpList);
           

        }
    }
}
